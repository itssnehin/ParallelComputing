import java.util.Scanner;
import java.io.*;
import java.util.concurrent.*;
import java.util.ArrayList;
import java.util.Arrays;
/**
 * 
 */
public class Simulation {


	public static void main(String[] args) {
		
		String fname, outname;

		if (args.length == 2) {

			fname = args[0];
			outname = args[1];

		} else {

			System.out.println("Error: Wrong format for args!");
			return; // Leave file
		}

		File file = new File (fname);
		File output = new File(outname);
		try{

			output.createNewFile();

		} catch (IOException e) {
			
			System.out.println("Error creating file!");
			return;
		}
		World world;
		Tree tree;
		Tree[] trees;
		System.out.println("Welcome!");
		try {

			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			String line;
            String[] details = new String[3];
            int i = 0;
			while (((line = br.readLine()) != null) && i <3) {
				details[i] = line;
                i++;
			}

            String[] size = details[0].split(" ");
            String[] sunlightString = details[1].split(" ");
            int numTrees = Integer.parseInt(details[2]);
            trees = new Tree[numTrees-1];

            //turn sunlight into float[][]
            int xsize = Integer.parseInt(size[0]);
            int ysize = Integer.parseInt(size[1]);
            float[][] sunlight = new float[xsize][ysize];
            int k = 0; // used to increment sting[]

            for (int j = 0; j < xsize ; j++ ) {
            	
            	for (int index = 0; index < ysize ; index++ ) {
            		sunlight[j][index] = Float.parseFloat(sunlightString[k++]);
            	}
            }


            world = new World(xsize, ysize, sunlight, numTrees);


            System.out.println("Working on Trees...");

            for (int l = 0; l < numTrees - 1; l++) {

            	line = br.readLine();
            	System.out.println("Line : " + line);
				String[] treeDetails = line.split(" "); //create array for each tree.
				int x = Integer.parseInt(treeDetails[0]);
				int y = Integer.parseInt(treeDetails[1]);
				int extend = Integer.parseInt(treeDetails[2]);

				tree = new Tree(x, y, extend);
				trees[l] = tree; //This will take a while :(
				i++;
            }
			

		} catch (IOException e) {
			System.out.println("Error reading file!");
			e.printStackTrace();
			return;
		}

		// Calculate the average sunlight across the world on the trees.
		System.out.println("Calculating stats...");
		float avg = world.getAverageSunlight(trees, world);
		float sql = world.getAverageSunlightSequential(trees, world);


		// get total sunlight for each tree
		float[] totalSunlight = world.totalSunlight;

		System.out.println(String.format("average: %.2f",  avg));

		//write to file output
		
		try {
			BufferedWriter out = new BufferedWriter(new FileWriter(output));
			out.write(String.format("%.2f", avg) + "");
			out.newLine();
			out.write(world.numTrees + "");
			out.newLine();

			for (float t : totalSunlight) {
				out.write(String.format("%.2f", t) + "");
				out.newLine();
			}

			out.flush();
			out.close();

		} catch(IOException e) {
			System.out.println("Error writing to file!");
			e.printStackTrace();
			return;
		}
		
		System.out.println("Done!");
		System.out.println("Threads: " + (Runtime.getRuntime().availableProcessors() + 1));
	}
	
}