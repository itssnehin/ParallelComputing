import java.util.concurrent.*;

/**
 * Class for summing an array
 */
public class SumArray extends RecursiveTask<Float> {

    int lo; int hi; float[] arr;
    int threads = Runtime.getRuntime().availableProcessors() + 1;
    int SEQUENTIAL_CUTOFF = 1000;
    long timeTaken;
    public SumArray(float[] arr, int lo, int hi) {

        this.arr = arr; this.lo = lo; this.hi = hi;

    }

    /**
     * Algorithm for computing the sum of an array using the divide and conquer strategy
     * overrides the compute method.
     * @return returns the total answer or a partial answer from the array.
     */
    protected Float compute() {

        if(hi - lo < SEQUENTIAL_CUTOFF) {
            float ans = 0;

            for (int i = lo; i < hi; i++) {
                    ans += arr[i];
               
            }
                return ans;


        } else {
            SumArray left = new SumArray(arr, lo, (hi+lo)/2);
            SumArray right = new SumArray(arr, (hi+lo)/2, hi);
            left.fork();
            float rightAns = right.compute();
            float leftAns = left.join();
            return leftAns+rightAns;
        }
    }

    public static final ForkJoinPool fjPool = new ForkJoinPool();

    /**
     * Use the fork join framework to solve the solution
     * @param arr the array of sunlight
     * @return returns the total sunlight.
     */
    
    public float sum(float[] arr) {

        float sum = ForkJoinPool.commonPool().invoke(new SumArray(arr,0, arr.length));
        return sum;
    }


}