import java.util.concurrent.*;

/**
 * Class for summing an array
 */
public class SumGrid extends RecursiveTask<Float> {

    float[][] grid;
    int extendX, extendY, startX;
    int SEQUENTIAL_CUTOFF = 1000;
    public SumGrid(float[][] grid, int startX, int extendX, int extendY) {

        this.grid = grid;
        this.extendX = extendX;
        this.extendY = extendY;
        this.startX = startX;
    }

    /**
     * Algorithm for computing the sum of an array using the divide and conquer strategy
     * overrides the compute method.
     * @return returns the total answer or a partial answer from the array.
     */
    protected Float compute() {

        if((extendX-startX)*extendY < SEQUENTIAL_CUTOFF) {
            
            float ans = 0;

            for (int i = startX; i < extendX ; i++ ) {
                
                for (int j = 0; j < extendY ; j++ ) {
                    
                    ans += grid[i][j];
                }
            }

            
            return ans;


        } else {
            SumGrid left = new SumGrid(grid, 0, extendX/2, extendY);
            SumGrid right = new SumGrid(grid, extendX/2, extendX, extendY);
            left.fork();
            float rightAns = right.compute();
            float leftAns = left.join();
            return leftAns+rightAns;
        }
    }

    public static final ForkJoinPool fjPool = new ForkJoinPool();

    /**
     * Use the fork join framework to solve the solution
     * @param grid the array of sunlight
     * @return returns the total sunlight.
     */
    public float sum(float[][] grid, int startX, int extendX, int extendY) {
        return ForkJoinPool.commonPool().invoke(new SumGrid(grid, startX, extendX, extendY));
    }

}