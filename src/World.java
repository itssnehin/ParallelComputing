import java.util.ArrayList;
import java.util.Arrays;

/**
 * Create a world object to store data from the input into
 */
public class World {

	int x, y;
	public float[][] grid;
	int numTrees;
	float[] totalSunlight;
	private long startTime;
	/**
	 * Constructor to initialise world object.
	 * @param  x,y     take the dimensions of the grid x * y
	 * @param  grid     the grid of sunlight on 1m x 1m blocks
	 * @param  numTrees the number of trees to input
	 * @param  treeList the list of all tree coordinates
	 */
	public World(int x,int y,float[][] grid,int numTrees) {

		// initialise variables
		this.x = x;
		this.y = y;
		this.grid = grid;
		this.numTrees = numTrees;

	}


	 public float getSunlight(Tree tree) {

       float[][] sunlight = getArraySunlight(tree);
       SumGrid sg = new SumGrid(sunlight, 0, tree.extend, tree.extend);
       float ans = sg.sum(sunlight,0,tree.extend,tree.extend);
       return ans;
    }




	/**
	 *
	 * @param xpos starting x position
	 * @param ypos starting y position
	 * @param extend the length of the square
	 * @return returns the array of sunlight values for a certain area in the grid.
	 */
	public float[][] getArraySunlight(Tree tree) {

		int xpos = tree.x;
		int ypos = tree.y;
		int extend = tree.extend;
		float[][] arr = new float[extend][extend];

		for (int i = 0; i < extend ; i++ ) {
			for (int j = 0; j < extend ; j++ ) {
				
				try {

					arr[i][j] = grid[xpos + i][ypos + j];

				} catch (IndexOutOfBoundsException | NullPointerException e) {
					continue;
				}
			}
		}
		return arr;
	}

	public int getNumTrees(){
		return numTrees;
	}

	/**
	 * Calculates the average sunlight using the fork join frame work
	 * @param  trees the list of trees.
	 * @param  world the details of the world simulated
	 * @return      [description]
	 */
	public float getAverageSunlight(Tree[] trees, World world) {

		//get average by calculating total sunlight and dividing by size of list
		// Total worked out using fork join framework.
		

		//Convert into float array of total sunlights (fork join used to calculate the total for each tree)
		totalSunlight = new float[trees.length];
		for (int i = 0; i < trees.length ; i++ ) {
			
			totalSunlight[i] = getSunSeq(trees[i]);
		}
		
		System.gc();
		tick();
		// Calculate the total sunlight for all trees using the fork join framework
		SumArray sa = new SumArray(totalSunlight, 0, totalSunlight.length);
		float total = sa.sum(totalSunlight);
		float time = tock();
		System.out.println("Total: " + String.format("%.2f", total));
		System.out.println("Time taken (parallel): " + time);
		return total/world.numTrees;
	}



	public float getAverageSunlightSequential(Tree[] trees, World  world) {
		
		totalSunlight = new float[trees.length];
		for (int i = 0; i < trees.length ; i++ ) {
			totalSunlight[i] = getSunSeq(trees[i]);
		}
		System.gc();
		tick();
		float total = 0;
		for (float item : totalSunlight ) {
			total += item;
		}
		float time = tock();

		//System.out.println("Total: " + String.format("%.2f", total));
		System.out.println("Time taken (sequential): " + time);

		return total/world.numTrees;
	}


	public float getSunSeq(Tree tree) {
		int extend = tree.extend;
		float[] arr = new float[extend*extend];
        float[][] sunlight = getArraySunlight(tree);
        float ans = 0;
        for(int i = 0; i < extend; i++) {

            for (int j = 0; j < extend; j++) {

                    ans += sunlight[i][j];
                }
            }
        

        return ans;

    }


	private void tick(){
		startTime = System.nanoTime();
	}
	private float tock(){
		return (System.nanoTime() - startTime) / 1000000000.0f; 
	}

}


